const express = require("express");
const fetch = require("node-fetch");
const open = require("open");

const { loadNuxt, build } = require("nuxt");
require("dotenv").config({ path: require("path").join(__dirname, "../.env") });

const app = express();

const isDev = process.env.NODE_ENV !== "production";

async function start() {
  try {
    // Init Nuxt.js
    const nuxt = await loadNuxt(isDev ? "dev" : "start");

    const { host, port } = nuxt.options.server;

    // Build only in dev mode
    if (isDev) {
      build(nuxt);
    } else {
      await nuxt.ready();
    }

    // Give nuxt middleware to express
    app.use(nuxt.render);

    // Listen the server
    app.listen(port, host);
    consola.ready({
      message: `Server listening on http://${host}:${port}`,
      badge: true,
    });

    await open(`http://${host}:${port}`);
    
  } catch (error) {
    console.log(error);
  }
}
start();
